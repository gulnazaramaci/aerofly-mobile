import 'package:aerofly/screens/menu_dashboard_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final Color backgroundColor = Color(0xFF203a91);
final Color greyColor = Color(0xFF9d9d9d);

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with SingleTickerProviderStateMixin{

  double ekranYuksekligi,ekranGenisligi;

  TabController tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 2,vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    ekranYuksekligi = MediaQuery.of(context).size.height;
    ekranGenisligi = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: backgroundColor,
      body: Container(
        width: ekranGenisligi,
        height: ekranYuksekligi,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/world.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.only(left: 10,right: 10,top: 8),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("AEROFLY",style: TextStyle(color: Colors.white,fontSize: 24,fontWeight: FontWeight.bold),),
                  ],
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 60),
                    padding: EdgeInsets.only(top: 20,bottom: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: <Widget>[
                        tabBarim(),
                        Expanded(
                          child: TabBarView(controller: tabController,children: <Widget>[
                            Container(
                              child: loginMethod(),
                            ),
                            Container(
                              child: signUpMethod(),
                            ),
                          ],),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ), /* add child content here */
      ),
    );
  }

  Widget signUpMethod(){
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: ListView(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
                hintText: "Ad Soyad",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          TextFormField(
            decoration: InputDecoration(
                hintText: "E-mail",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Parola",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Kayıt Ol",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: backgroundColor,
            onPressed: (){},
          ),
          SizedBox(height: 15,),
          Row(children: <Widget>[
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only( right: 20.0),
                  child: Divider(
                    color: Colors.grey.shade400,
                    height: 36,
                  )),
            ),
            Text("veya",style: TextStyle(color: Colors.grey.shade400),),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0),
                  child: Divider(
                    color: Colors.grey.shade400,
                    height: 36,
                  )),
            ),
          ]),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Facebook İle Kayıt Ol",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: Colors.indigo,
            onPressed: (){},
          ),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Google İle Kayıt Ol",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: Colors.red,
            onPressed: (){},
          ),
        ],
      ),
    );
  }

  Widget loginMethod() {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: ListView(
        children: <Widget>[
            TextFormField(
              decoration: InputDecoration(
                hintText: "E-mail",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
              ),
            ),
            SizedBox(height: 15,),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Parola",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Giriş Yap",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: backgroundColor,
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>MenuDashboard()));
            },
          ),
          SizedBox(height: 15,),
          Row(children: <Widget>[
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only( right: 20.0),
                  child: Divider(
                    color: Colors.grey.shade400,
                    height: 36,
                  )),
            ),
            Text("veya",style: TextStyle(color: Colors.grey.shade400),),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0),
                  child: Divider(
                    color: Colors.grey.shade400,
                    height: 36,
                  )),
            ),
          ]),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Facebook İle Giriş Yap",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: Colors.indigo,
            onPressed: (){},
          ),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Google İle Giriş Yap",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: Colors.red,
            onPressed: (){},
          ),
        ],
      ),
    );
  }

  Widget tabBarim() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide( //                   <--- left side
            color: greyColor,
            width: 1.0,
          ),
        ),
      ),
      child: TabBar(
        controller: tabController,
        unselectedLabelColor: greyColor,
        labelColor: backgroundColor,
        labelStyle: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),
        tabs: <Widget>[
        Tab(text: "Giriş Yap"),
        Tab(text:  "Kayıt Ol"),
      ],),
    );
  }



}
