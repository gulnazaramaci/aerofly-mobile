import 'package:aerofly/component/side_menu.dart';
import 'package:aerofly/screens/ucus_listesi_widget.dart';
import 'package:flutter/material.dart';

final Color backgroundColor = Color(0xFF203a91);

class MenuDashboard extends StatefulWidget {
  @override
  _MenuDashboardState createState() => _MenuDashboardState();
}

class _MenuDashboardState extends State<MenuDashboard> {

  double ekranYuksekligi,ekranGenisligi;
  bool menuAcikmi = false;

  @override
  Widget build(BuildContext context) {

    ekranYuksekligi = MediaQuery.of(context).size.height;
    ekranGenisligi = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            menuOlustur(context),
             dashboardOlustur(context),
          ],
        ),
      ),
    );
  }

  Widget menuOlustur(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:15.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child:SideMenu()
      ),
    );
  }

  Widget dashboardOlustur(BuildContext context) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 500),
      top:menuAcikmi ? 0.2 * ekranYuksekligi : 0,
      bottom:menuAcikmi ? 0.2 * ekranYuksekligi : 0,
      left:menuAcikmi ? 0.7 * ekranGenisligi : 0,
      right:menuAcikmi ? -0.5 * ekranGenisligi : 0,
      child: Material(
        color: backgroundColor,
        elevation: menuAcikmi ? 8 : 0,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/world.png"),
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.only(left: 10,right: 10,top: 8),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                      onTap: (){
                        setState(() {
                          menuAcikmi = !menuAcikmi;
                        });
                      },
                      child: menuAcikmi ? Icon(Icons.close,color: Colors.white,size: 30,) : Icon(Icons.menu,color: Colors.white,size: 30,),),
                  Text("AEROFLY",style: TextStyle(color: Colors.white,fontSize: 24),),
                  Icon(Icons.more_vert,color: Colors.white,size: 30,),
                ],
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 60),
                  padding: EdgeInsets.only(top: 20,bottom: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: biletAlForm(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget biletAlForm(){
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: ListView(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
                hintText: "Nereden",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Nereye",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Gidiş Tarihi",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Dönüş Tarihi",
                border: OutlineInputBorder(),
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade300))
            ),
          ),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Bilet Bul",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: backgroundColor,
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>UcusListesi()));
            },
          ),
        ],
      ),
    );
  }
}
