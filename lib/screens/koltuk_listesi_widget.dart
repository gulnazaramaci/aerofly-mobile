import 'package:aerofly/component/side_menu.dart';
import 'package:aerofly/screens/bilet_sayfasi_widget.dart';
import 'package:flutter/material.dart';

class KoltukListesi extends StatefulWidget {
  @override
  _KoltukListesiState createState() => _KoltukListesiState();
}

class _KoltukListesiState extends State<KoltukListesi> {

  double ekranYuksekligi,ekranGenisligi;
  bool menuAcikmi = false;
  var secilenKoltuk = "";

  @override
  Widget build(BuildContext context) {

    ekranYuksekligi = MediaQuery.of(context).size.height;
    ekranGenisligi = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            menuOlustur(context),
            dashboardOlustur(context),
          ],
        ),
      ),
    );
  }

  koltukSec(gelenKoltuk){
    setState(() {
      secilenKoltuk=gelenKoltuk;
    });
    print(secilenKoltuk);
  }

  biletSayfasinaGit(){
    if(secilenKoltuk != ""){
      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>BiletSayfasi()));
    }else{
      _showMyDialog();
    }
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Uyarı'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Lütfen Koltuk Seçiniz'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Tamam'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget menuOlustur(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:15.0),
      child: Align(
          alignment: Alignment.centerLeft,
          child:SideMenu()
      ),
    );
  }

  Widget dashboardOlustur(BuildContext context) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 500),
      top:menuAcikmi ? 0.2 * ekranYuksekligi : 0,
      bottom:menuAcikmi ? 0.2 * ekranYuksekligi : 0,
      left:menuAcikmi ? 0.7 * ekranGenisligi : 0,
      right:menuAcikmi ? -0.5 * ekranGenisligi : 0,
      child: Material(
        color: backgroundColor,
        elevation: menuAcikmi ? 8 : 0,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/world.png"),
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.only(left: 10,right: 10,top: 8),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      setState(() {
                        menuAcikmi = !menuAcikmi;
                      });
                    },
                    child: menuAcikmi ? Icon(Icons.close,color: Colors.white,size: 30,) : Icon(Icons.menu,color: Colors.white,size: 30,),),
                  Text("AEROFLY",style: TextStyle(color: Colors.white,fontSize: 24),),
                  Icon(Icons.more_vert,color: Colors.white,size: 30,),
                ],
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 60),
                  padding: EdgeInsets.only(top: 20,bottom: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: koltukListesi(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Padding koltukListesi(){
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: ListView(
        children: <Widget>[
          Center(
            child: Text("Koltuk Seçiniz",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
          ),
          SizedBox(height: 20,),
          Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("1");},
                      child: secilenKoltuk != "1" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("2");},
                      child: secilenKoltuk != "2" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("3");},
                      child: secilenKoltuk != "3" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("4");},
                      child: secilenKoltuk != "4" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("5");},
                      child: secilenKoltuk != "5" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("6");},
                      child: secilenKoltuk != "6" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("7");},
                      child: secilenKoltuk != "7" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("8");},
                      child: secilenKoltuk != "8" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("9");},
                      child: secilenKoltuk != "9" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){},
                      child: Image(image: AssetImage('assets/dolu-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("11");},
                      child: secilenKoltuk != "11" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("12");},
                      child: secilenKoltuk != "12" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("13");},
                      child: secilenKoltuk != "13" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("14");},
                      child: secilenKoltuk != "14" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){},
                      child: Image(image: AssetImage('assets/dolu-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("16");},
                      child: secilenKoltuk != "16" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){},
                      child: Image(image: AssetImage('assets/dolu-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("18");},
                      child: secilenKoltuk != "18" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("19");},
                      child: secilenKoltuk != "19" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("20");},
                      child: secilenKoltuk != "20" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("21");},
                      child: secilenKoltuk != "21" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("22");},
                      child: secilenKoltuk != "22" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("23");},
                      child: secilenKoltuk != "23" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("24");},
                      child: secilenKoltuk != "24" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("25");},
                      child: secilenKoltuk != "25" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("26");},
                      child: secilenKoltuk != "26" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                  Row(children: <Widget>[
                    InkWell(
                      onTap: (){koltukSec("27");},
                      child: secilenKoltuk != "27" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                    SizedBox(width: 15,),
                    InkWell(
                      onTap: (){koltukSec("28");},
                      child: secilenKoltuk != "28" ? Image(image: AssetImage('assets/bos-koltuk.png')) : Image(image: AssetImage('assets/secilen-koltuk.png')),
                    ),
                  ],),
                ],
              ),
            ],
          ),
          SizedBox(height: 15,),
          RaisedButton(
            padding: EdgeInsets.only(top: 20,bottom: 20),
            child: Text("Bileti Al",style: TextStyle(fontSize: 18,color: Colors.white),),
            color: Colors.indigo,
            onPressed: (){
             biletSayfasinaGit();
            },
          ),
        ],
      ),
    );
  }


}
