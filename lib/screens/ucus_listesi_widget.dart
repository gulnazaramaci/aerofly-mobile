import 'package:aerofly/component/side_menu.dart';
import 'package:aerofly/screens/koltuk_listesi_widget.dart';
import 'package:flutter/material.dart';

class UcusListesi extends StatefulWidget {
  @override
  _UcusListesiState createState() => _UcusListesiState();
}

class _UcusListesiState extends State<UcusListesi> {

  double ekranYuksekligi,ekranGenisligi;
  bool menuAcikmi = false;

  @override
  Widget build(BuildContext context) {

    ekranYuksekligi = MediaQuery.of(context).size.height;
    ekranGenisligi = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            menuOlustur(context),
            dashboardOlustur(context),
          ],
        ),
      ),
    );
  }

  Widget menuOlustur(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:15.0),
      child: Align(
          alignment: Alignment.centerLeft,
          child:SideMenu()
      ),
    );
  }

  Widget dashboardOlustur(BuildContext context) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 500),
      top:menuAcikmi ? 0.2 * ekranYuksekligi : 0,
      bottom:menuAcikmi ? 0.2 * ekranYuksekligi : 0,
      left:menuAcikmi ? 0.7 * ekranGenisligi : 0,
      right:menuAcikmi ? -0.5 * ekranGenisligi : 0,
      child: Material(
        color: backgroundColor,
        elevation: menuAcikmi ? 8 : 0,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/world.png"),
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.only(left: 10,right: 10,top: 8),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      setState(() {
                        menuAcikmi = !menuAcikmi;
                      });
                    },
                    child: menuAcikmi ? Icon(Icons.close,color: Colors.white,size: 30,) : Icon(Icons.menu,color: Colors.white,size: 30,),),
                  Text("AEROFLY",style: TextStyle(color: Colors.white,fontSize: 24),),
                  Icon(Icons.more_vert,color: Colors.white,size: 30,),
                ],
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 60),
                  padding: EdgeInsets.only(top: 20,bottom: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                  ),
                  child: UcusContainer(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Padding UcusContainer(){
    return Padding(
        padding: const EdgeInsets.all(5.0),
        child: ListView(
          children: <Widget>[
            Center(
              child: Text("Uçus Seçiniz",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            ),
            SizedBox(height: 20,),
            FlatButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>KoltukListesi()));
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
            Divider(
              color: Colors.grey.shade300,
              height: 20,
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            FlatButton(
              onPressed: () {
                /*...*/
              },
              child: ListTile(
                leading: Icon(Icons.room, size: 40),
                title: Text('Sabiha Gökçen Hava Limanı'),
                subtitle: Text('06.04.2020 15:18'),
              ),
            ),
          ],
        ),
    );
  }
}
