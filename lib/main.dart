import 'package:aerofly/screens/login_widget.dart';
//import 'package:aerofly/screens/menu_dashboard_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  final Color backgroundColor = Color(0xFF203a91);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: backgroundColor,
    statusBarIconBrightness: Brightness.light
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aerofly',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: LoginScreen()
    );
  }
}

