import 'package:aerofly/screens/login_widget.dart';
import 'package:aerofly/screens/menu_dashboard_widget.dart';
import 'package:flutter/material.dart';

final TextStyle menuFontStyle = TextStyle(color: Colors.white,fontSize:18,fontWeight: FontWeight.w300);
final Color whiteColor = Color(0xFFd1d2ec);
final Color backgroundColor = Color(0xFF203a91);

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: FractionalTranslation(
                  translation: Offset(0.0, 0.0),
                  child: Align(
                    child: CircleAvatar(
                      radius: 35.0,
                      child: Text("J"),
                    ),
                    alignment: FractionalOffset(0.5, 0.0),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment : CrossAxisAlignment.start,
                children: <Widget>[
                  Text("John Dicola",style: TextStyle(color: Colors.white,fontSize:20),),
                  SizedBox(height: 1),
                  Text("Australia",style: TextStyle(color: Colors.indigo.shade200,fontSize:15),),
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>MenuDashboard()));
          },
          icon: Container(
          width: 45,
          height: 45,
          margin: EdgeInsets.only(right: 10),
          decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(100),),
          child: Transform.rotate(
            angle: 45,
            child: IconButton(
              icon: Icon(
                Icons.flight,
                color: backgroundColor,
              ),
              onPressed: null,
            ),
          ),),
          label: Text("Bilet Al", style: menuFontStyle,),),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){},
          icon: Container(
            width: 45,
            height: 45,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(100),),
            child: Transform.rotate(
              angle: 0,
              child: IconButton(
                icon: Icon(
                  Icons.watch_later,
                  color: backgroundColor,
                ),
                onPressed: null,
              ),
            ),),
          label: Text("Uçuş Durumu", style: menuFontStyle,),),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){},
          icon: Container(
            width: 45,
            height: 45,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(100),),
            child: Transform.rotate(
              angle: 0,
              child: IconButton(
                icon: Icon(
                  Icons.transfer_within_a_station,
                  color: backgroundColor,
                ),
                onPressed: null,
              ),
            ),),
          label: Text("Seyehat Bilgisi", style: menuFontStyle,),),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){},
          icon: Container(
            width: 45,
            height: 45,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(100),),
            child: Transform.rotate(
              angle: 0,
              child: IconButton(
                icon: Icon(
                  Icons.book,
                  color: backgroundColor,
                ),
                onPressed: null,
              ),
            ),),
          label: Text("Şartlar & Koşullar", style: menuFontStyle,),),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){},
          icon: Container(
            width: 45,
            height: 45,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(100),),
            child: Transform.rotate(
              angle: 0,
              child: IconButton(
                icon: Icon(
                  Icons.notifications_active,
                  color: backgroundColor,
                ),
                onPressed: null,
              ),
            ),),
          label: Text("Bildirimler", style: menuFontStyle,),),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){},
          icon: Container(
            width: 45,
            height: 45,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(100),),
            child: Transform.rotate(
              angle: 0,
              child: IconButton(
                icon: Icon(
                  Icons.contact_phone,
                  color: backgroundColor,
                ),
                onPressed: null,
              ),
            ),),
          label: Text("İletişim", style: menuFontStyle,),),
        SizedBox(height: 20,),
        FlatButton.icon(
          onPressed: (){
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>LoginScreen()));
          },
          icon: Container(
            width: 45,
            height: 45,
            margin: EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.circular(100),),
            child: Transform.rotate(
              angle: 0,
              child: IconButton(
                icon: Icon(
                  Icons.exit_to_app,
                  color: backgroundColor,
                ),
                onPressed: null,
              ),
            ),),
          label: Text("Çıkış yap", style: menuFontStyle,),),
      ],
    );
  }
}

